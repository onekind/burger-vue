import { defineConfig } from 'vite'
import { resolve } from 'path'
import vue from '@vitejs/plugin-vue'
import { visualizer } from 'rollup-plugin-visualizer'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    // https://github.com/btd/rollup-plugin-visualizer
    {...(process.env.ANALYZE && visualizer({
      open: true,
      title: 'Burger Vue',
      template: process.env.ANALYZETEMPLATE || 'treemap',
      gzipSize: true,
    }))},
  ],
  build: {
    lib: {
      entry: resolve(__dirname, 'src/index.js'),
      fileName: (format) => `burger-vue.${format}.js`,
      // formats: ['es'],
      name: 'burger-vue',
    },
    rollupOptions: {
      // make sure to externalize deps that shouldn't be bundled
      // into your library
      external: ['vue'],
      output: {
        // Provide global variables to use in the UMD build
        // for externalized deps
        globals: {
          vue: 'Vue',
        },
      },
    },
  },
})
