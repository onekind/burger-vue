# Get Started

## Installing the component

```bash
yarn add @onekind/burger-vue
```

## Importing

### Component import

Add all the components, including the external ones to your main project file.

```js
import BurgerVue from '@onekind/burger-vue'

app.use(BurgerVue)
```

## Options

There are a few options to customize how the component behaves:

- `flavour`: define which burger animation it should render
- `thickness`: arbitrary number to define the thinkness of the graphics. Default at `3.5`
- `thick` or `thin`: boolean flags which allow to immediately influence the thickness (`2.5` or `5.5`)
- `color`: a string which is passed down directly to the component color styling
