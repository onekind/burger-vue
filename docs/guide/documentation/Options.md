# Flavours

<Sample title="thick">
  <BurgerThick  />
</Sample>

<<< @/examples/BurgerThick.vue

<Sample title="thin">
  <BurgerThin />
</Sample>

<<< @/examples/BurgerThin.vue

<Sample title="color">
  <BurgerColor  />
</Sample>

<<< @/examples/BurgerColor.vue

<script setup>
import BurgerThick from '../../examples/BurgerThick.vue'
import BurgerThin from '../../examples/BurgerThin.vue'
import BurgerColor from '../../examples/BurgerColor.vue'
</script>
