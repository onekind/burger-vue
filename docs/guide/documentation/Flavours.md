# Flavours

<Sample title="cross-right">
  <BurgerCrossRight  />
</Sample>

<<< @/examples/BurgerCrossRight.vue

<Sample title="arrow-top-left">
  <BurgerArrowTopLeft />
</Sample>

<<< @/examples/BurgerArrowTopLeft.vue

<Sample title="close">
  <BurgerClose  />
</Sample>

<<< @/examples/BurgerClose.vue

<Sample title="plus">
  <BurgerPlus  />
</Sample>

<<< @/examples/BurgerPlus.vue

<Sample title="arrow-right">
  <BurgerArrowRight  />
</Sample>

<<< @/examples/BurgerArrowRight.vue

<Sample title="cross-complex">
  <BurgerCrossComplex  />
</Sample>

<<< @/examples/BurgerCrossComplex.vue

<Sample title="cross-left">
  <BurgerCrossLeft  />
</Sample>

<<< @/examples/BurgerCrossLeft.vue

<Sample title="cross-rotate">
  <BurgerCrossRotate  />
</Sample>

<<< @/examples/BurgerCrossRotate.vue

<script setup>
import BurgerCrossRight from '../../examples/BurgerCrossRight.vue'
import BurgerArrowTopLeft from '../../examples/BurgerArrowTopLeft.vue'
import BurgerClose from '../../examples/BurgerClose.vue'
import BurgerPlus from '../../examples/BurgerPlus.vue'
import BurgerArrowRight from '../../examples/BurgerArrowRight.vue'
import BurgerCrossComplex from '../../examples/BurgerCrossComplex.vue'
import BurgerCrossLeft from '../../examples/BurgerCrossLeft.vue'
import BurgerCrossRotate from '../../examples/BurgerCrossRotate.vue'
</script>
