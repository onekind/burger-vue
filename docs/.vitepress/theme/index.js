import DefaultTheme from 'vitepress/theme'
import Burger from '../../../src/'
import Sample from '../../components/Sample.vue'

export default {
  ...DefaultTheme,
  enhanceApp({ app }) {
    app.component('Sample', Sample)
    app.use(Burger)
  },
}
