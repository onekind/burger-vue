## [1.1.2](https://gitlab.com/onekind/burger-vue/compare/v1.1.1...v1.1.2) (2022-09-11)


### Bug Fixes

* docs not building ([9fc907c](https://gitlab.com/onekind/burger-vue/commit/9fc907c2211d5152ada32108d754dbb758cc9fc9))

## [1.1.1](https://gitlab.com/onekind/burger-vue/compare/v1.1.0...v1.1.1) (2022-09-09)


### Bug Fixes

* remove unecessary dep ([e16b914](https://gitlab.com/onekind/burger-vue/commit/e16b914c013d3435f35a5b8c9f44cea3ef8cd506))

# [1.1.0](https://gitlab.com/onekind/burger-vue/compare/v1.0.4...v1.1.0) (2022-09-09)


### Features

* new vue 3 implementation ([3423137](https://gitlab.com/onekind/burger-vue/commit/3423137852ad5746f8177507e25d855011371b96))

## [1.0.4](https://gitlab.com/onekind/burger-vue/compare/v1.0.3...v1.0.4) (2022-06-24)


### Bug Fixes

* update README.md badges ([fa4056d](https://gitlab.com/onekind/burger-vue/commit/fa4056decf177042edfb97df04cb0f434693771a))

## [1.0.3](https://gitlab.com/onekind/burger-vue/compare/v1.0.2...v1.0.3) (2022-06-23)


### Bug Fixes

* logos not showing in demo page ([2e26845](https://gitlab.com/onekind/burger-vue/commit/2e26845d3d2ea1522c00cd2f388a16dd6d1ec842))
* remove console.log ([0d559c4](https://gitlab.com/onekind/burger-vue/commit/0d559c4bbcae4565ec531c57298e53861567bfb3))
* validator for burger-menu ([761d1f2](https://gitlab.com/onekind/burger-vue/commit/761d1f26a436d19c94c1e8e5400b975b0b34341a))

## [1.0.2](https://gitlab.com/onekind/burger-vue/compare/v1.0.1...v1.0.2) (2022-06-16)


### Bug Fixes

* author email address ([a81cc43](https://gitlab.com/onekind/burger-vue/commit/a81cc43453d54cac2ebfad761c346f0a7f6d219c))
* eslint setup ([fb444b2](https://gitlab.com/onekind/burger-vue/commit/fb444b242593aaa5a41a9a3354810e4eb47ac084))

## [1.0.1](https://gitlab.com/onekind/burger-vue/compare/v1.0.0...v1.0.1) (2022-04-20)


### Bug Fixes

* prevent button from overflowing ([fa8df69](https://gitlab.com/onekind/burger-vue/commit/fa8df6979f044a8d48b6e54248a6b9a77847aad3))

# 1.0.0 (2022-01-22)


### Features

* new pages demonstration ([1f82c69](https://gitlab.com/onekind/burger-vue/commit/1f82c690ed1c60b8a65ec6bf58348ab9a0e43d00))
