# @onekind/burger-vue

![logo](https://gitlab.com/onekind/burger-vue/raw/master/docs/public/logo.svg)

[![build status](https://img.shields.io/gitlab/pipeline/onekind/burger-vue/master.svg?style=for-the-badge)](https://gitlab.com/onekind/burger-vue.git)
[![npm-publish](https://img.shields.io/npm/dm/@onekind/burger-vue.svg?style=for-the-badge)](https://www.npmjs.com/package/@onekind/burger-vue)
[![release](https://img.shields.io/npm/v/@onekind/burger-vue?label=%40onekind%2Fburger-vue%40latest&style=for-the-badge)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg?style=for-the-badge)](https://github.com/semantic-release/semantic-release)

A Vue JSON editor

Checkout the [Demo](https://onekind.gitlab.io/burger-vue/) which contains the component documentation.

> If you enjoy this component, feel free to drop me feedback, either via the repository, or via jose@onekind.io.

## Instalation

```bash
yarn add @onekind/burger-vue
```

## Setup

### Vue

- Add the following to you application main.js file:

```js
import {Burger} from '@onekind/burger-vue'

Vue.use(Burger)
```
