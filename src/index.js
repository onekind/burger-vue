import components from'./components'

const plugin = {
  install (Vue) {
    Object.keys(components).forEach((key) => {
      const component = components[key]
      Vue.component(key, component)
    })
  },
}

export default plugin
